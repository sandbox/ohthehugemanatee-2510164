<?php
/**
 * @file
 * Contains \Drupal\box_api_sitewide\BoxApiSitewide|BoxFile.
 */

namespace Drupal\box_api_sitewide\BoxApiSitewide;

/**
 * Class BoxFile.
 * @package Drupal\box_api_sitewide\BoxApiSitewide
 *
 * Represents a File in Box.
 */
class BoxFile extends BoxObjectBase {
  /**
   * @inheritdoc
   */
  public function __construct($param, &$connection) {
    parent::__construct($param, $connection);
    $this->boxType = 'file';
  }

  /**
   * Get the actual file contents.
   */
  public function getContents() {
    return $this->connection->request('file_content', $this->boxId);
  }


  /**
   * @inheritdoc
   */
  protected function loadFromListing(\stdClass $object) {
    parent::loadFromListing($object);
    if ($object->modified_at) {
      $this->updated = strtotime($object->modified_at);
    }
  }

  /**
   * @var int
   *   Timestamp from the last time this file was updated in Box.
   */
  public $updated;

  /**
   * Timestamp getter.
   */
  public function getUpdated() {
    if (empty($this->updated)) {
      $this->loadFromBox();
    }
    return $this->updated;
  }
} 