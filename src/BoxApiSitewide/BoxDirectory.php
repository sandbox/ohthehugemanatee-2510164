<?php
/**
 * @file
 * Contains \Drupal\box_api_sitewide\BoxApiSitewide|BoxDirectory.
 */

namespace Drupal\box_api_sitewide\BoxApiSitewide;

/**
 * Class BoxDirectory
 * @package Drupal\box_api_sitewide\BoxApiSitewide
 *
 * Represents a Directory in Box.
 */
class BoxDirectory extends BoxObjectBase {

  /**
   * @var array
   *   the directory listing
   */
  public $listing;

  /**
   * @var array
   *   a recursive listing of all files within this directory and subdirs.
   */
  public $recursiveListing;

  /**
   * @inheritdoc
   */
  public function __construct($param = '0', &$connection) {
    parent::__construct($param, $connection);
    $this->boxType = 'folder';
  }

  /**
   * @inheritdoc
   */
  protected function loadFromListing(\stdClass $object) {
    parent::loadFromListing($object);
    // Directory Listing.
    $this->listing = array();
    foreach ($object->item_collection->entries as $entry) {
      if ($entry->type == 'folder') {
        $this->listing[$entry->id] = new BoxDirectory($this->connection);
        $this->listing[$entry->id]->load($entry);
      }
      else {
        $this->listing[$entry->id] = new BoxFile($this->connection);
        $this->listing[$entry->id]->load($entry);
      }
    }
  }

  /**
   * Get a listing of directory contents.
   * @return array
   *   Array of directory contents.
   */
  public function getListing() {
    if (empty($this->listing)) {
      $this->loadFromBox();
    }
    return $this->listing;
  }

  /**
   * @return array
   *   Array of files contained in this dir and its subdirs.
   */
  public function getRecursiveListing() {
    if (empty($this->recursiveListing)) {
      $this->recursiveListing = $this->recursiveListingGenerator($this);
    }
    return $this->recursiveListing;
  }

  /**
   * Get the recursive listing from box.
   * @var BoxDirectory
   *   A directory object.
   */
  private function recursiveListingGenerator($directory) {
    // Start with an empty list, local to this iteration.
    $recursiveListing = array();
    if ($directory->getListing()) {
      foreach ($directory->getListing() as $entry) {
        if ($entry->boxType == 'file') {
          // If it's a file, add it to the list.
          $recursiveListing[$entry->boxId] = $entry;
        }
        else {
          // If it's a directory, recurse into it.
          $recursiveListing += $this->recursiveListingGenerator($entry);
        }
      }
      return $recursiveListing;
    }
    else {
      return FALSE;
    }
  }

} 