<?php
/**
 * @file
 * Contains \Drupal\box_api_sitewide\BoxApiSitewide|BoxObjectBase.
 */

namespace Drupal\box_api_sitewide\BoxApiSitewide;

/**
 * Abstract Class BoxDirectory
 * @package Drupal\box_api_sitewide\BoxApiSitewide
 *
 * Represents a Directory in Box.
 */
abstract class BoxObjectBase {
  /**
   * @var string
   *   Box unique identifying string.
   */
  protected $boxId;

  /**
   * Getter for boxId
   */
  public function getBoxId() {
    return $this->boxId;
  }

  /**
   * @var string
   *   The box object type, 'file' or 'folder'.
   */
  protected $boxType;

  /**
   * Getter for boxType
   */
  public function getBoxType() {
    return $this->boxType;
  }

  /**
   * @var BoxConnection
   *   The connection object used to handle this object.
   */
  protected $connection;

  /**
   * @var string
   *   The title of the directory
   */
  protected $title;

  /**
   * @var array
   *   the parent directory.
   */
  protected $parent;

  /**
   * @var array
   *   The full string of parent directories.
   */
  protected $parents;

  /**
   * Constructor
   * @param BoxConnection $connection
   *   The box connection object to be used.
   */
  public function __construct(BoxConnection &$connection) {
    $this->connection = $connection;
    $this->connection->authorize();
  }

  /**
   * Sets object ID. Child constructors must set the type.
   *
   * @param mixed $param
   *   Either an object ID, or a directory object as returned from Box.
   */
  public function load($param) {
    // If the first parameter is a string, assume a box ID.
    if (is_string($param)) {
      $this->boxId = $param;
    }
    else {
      // Otherwise, assume a listing object as returned from the box API.
      if (isset($param->id)) {
        $this->boxId = $param->id;
        $this->boxType = $param->type;
        // Set other attributes.
        $this->loadFromListing($param);
      }
      else {
        throw new \Exception('Box objects must be instantiated with an ID');
      }
    }
  }

  /**
   * Load attributes into this object from a listing object.
   * This is the method to override if you have other attributes to include. You
   * will probably want to get these attributes with parent::loadFromListing($object)
   * at the top of your method.
   * $var StdClass
   *   a result object as returned from box.
   */
  protected function loadFromListing(\stdClass $object) {
    $this->title = isset($object->name) ? $object->name : NULL;
    $this->parent = isset ($object->parent) ? new BoxDirectory($object->parent, $this->connection) : NULL;
    if (isset($object->path_collection->entries)) {
      $this->parents = array();
      foreach ($object->path_collection->entries as $key => $directory) {
        $this->parents[$key] = new BoxDirectory($this->connection);
        $this->parents[$key]->load($directory);
      }
    }
  }

  /**
   * Load attributes into this object from the Box API.
   */
  public function loadFromBox() {
    $return = $this->connection->request($this->boxType, $this->boxId);
    if ($return) {
      $this->loadFromListing($return);
    }
  }

  /**
   * Get the title.
   * @return string
   */
  public function getTitle() {
    if (empty($this->title)) {
      $this->loadFromBox();
    }
    return $this->title;
  }

  /**
   * Get the parent directory
   * @return BoxDirectory
   */
  public function getParent() {
    if (empty($this->parent)) {
      $this->loadFromBox();
    }
    return $this->parent;
  }

  /**
   * Get the parent directories.
   * @return array
   *   the full set of parent directories, starting from root (0).
   */
  public function getParents() {
    if (empty($this->parents)) {
      $this->loadFromBox();
    }
    return $this->parents;
  }

} 