<?php

/**
 * @file
 * Contains \Drupal\box_api_sitewide\ServiceContainer\ServiceProvider\BoxApiSitewideServiceProvider
 */

namespace Drupal\box_api_sitewide\ServiceContainer\ServiceProvider;

use Drupal\service_container\DependencyInjection\ServiceProviderInterface;

/**
 * Provides box api sitewide service definitions.
 *
 * @codeCoverageIgnore
 */
class BoxApiSitewideServiceProvider implements ServiceProviderInterface {

  /**
   * Gets a service container definition.
   *
   * @return array
   *   Returns an associative array with the following keys:
   *     - parameters: Simple key-value store of container parameters
   *     - services: Services like defined in services.yml
   *   factory methods, arguments and tags are supported for services.
   *
   * @see core.services.yml in Drupal 8
   */
  public function getContainerDefinition() {
    return array(
      'parameters',
      'services' => array(
        'box_api_sitewide.connection' => array(
          'class' => '\Drupal\box_api_sitewide\BoxApiSitewide\BoxConnection',
          'arguments' => array('@variable', '@box_api_sitewide.oauth2'),
          'tags' => array(
            array('name' => 'box_api_sitewide.connection'),
          ),
        ),
        'box_api_sitewide.file' => array(
          'class' => '\Drupal\box_api_sitewide\BoxApiSitewide\BoxFile',
          'arguments' => array('@box_api_sitewide.connection'),
          'tags' => array(
            array('name' => 'box_api_sitewide.file'),
          ),
        ),
        'box_api_sitewide.directory' => array(
          'class' => '\Drupal\box_api_sitewide\BoxApiSitewide\BoxDirectory',
          'arguments' => array('@box_api_sitewide.connection'),
          'tags' => array(
            array('name' => 'box_api_sitewide.directory'),
          ),
        ),
        'box_api_sitewide.oauth2' => array(
          'class' => '\Drupal\box_api_sitewide\BoxApiSitewide\OAuth2\Client',
          'arguments' => array('@variable'),
          'tags' => array(
            array('name' => 'box_api_sitewide.oauth2'),
          ),
        ),
      ),
    );
  }
  public function alterContainerDefinition(&$container_definition) {}
}