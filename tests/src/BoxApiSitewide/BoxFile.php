<?php

/**
 * @file
 * Contains \Drupal\box_api_sitewide\Tests\BoxApiSitewide\BoxConnectionTest
 */

namespace Drupal\box_api_sitewide\Tests\BoxApiSitewide;

use Drupal\box_api_sitewide\BoxApiSitewide\BoxConnection;
use Drupal\service_container\Variable;

/**
 * @coversDefaultClass \Drupal\box_api_sitewide\BoxApiSitewide\BoxConnection
 * @group custom
 */
class BoxFileTest extends \PHPUnit_Framework_TestCase {

  /**
   * The tested File.
   */
  protected $boxFile;

  /**
   * The mock BoxConnection.
   */
  protected $boxConnection;

  /**
   * A sample valid file object.
   */
  protected $sampleFile;

  /**
   * setUp - set up our environment.
   */
  public function setUp() {
    // The sample file.
    $this->sampleFile = array();

    // Load up a mock BoxConnection
    $this->boxConnection = $this->getMock(BoxConnection::class, ['request'])
      ->method('request')
      ->willreturn('response');
  }

  /**
   * @covers __construct()
   */
  public function testConstructor() {
  }
}
