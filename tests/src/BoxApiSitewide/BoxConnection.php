<?php

/**
 * @file
 * Contains \Drupal\box_api_sitewide\Tests\BoxApiSitewide\BoxConnectionTest
 */

namespace Drupal\box_api_sitewide\Tests\BoxApiSitewide;

use Drupal\box_api_sitewide\BoxApiSitewide\BoxConnection;
use Drupal\box_api_sitewide\BoxApiSitewide\OAuth2\Client;
use Drupal\service_container\Variable;
use Mockery;

/**
 * @coversDefaultClass \Drupal\box_api_sitewide\BoxApiSitewide\BoxConnection
 * @group custom
 */
class BoxConnectionTest extends \PHPUnit_Framework_TestCase {

  /**
   * The tested Connection.
   */
  protected $boxConnection;

  /**
   * The mock variable service.
   */
  protected $variable;

  /**
   * The mock OAuth2 Client.
   */
  protected $oAuth2Client;

  /**
   * A sample valid token.
   */
  protected $sampleToken;

  /**
   * setUp - set up our environment.
   */
  public function setUp() {
    // The sample token.
    $this->sampleToken = array(
      'access_token' => '3ynwzEzqoJI1nCyrxjBEeCvffecX1iDd',
      'expires_in' => '3777',
      'restricted_to' => array(),
      'refresh_token' => 'XY3OLGBeZvtm5VxUKzeUw27ofVmmgnoyOKM2kFKzHab5HgHq0evzt3VqfzWFHXNU',
      'token_type' => 'bearer',
      'expiration_time' => time() + 36000,
    );

    // The mock variable service. Only useful for getting values listed here.
    $this->variable = \Mockery::mock('Drupal\service_container\Variable');
    $this->variable->shouldReceive('get');

    // Load up a mock OAuth2Client. StdClass because PHPUnit can't include the base class.
    $this->oAuth2Client = \Mockery::mock('Drupal\box_api_sitewide\BoxApiSitewide\OAuth2\Client');
    $this->oAuth2Client->shouldReceive('getAccessToken')->andReturn($this->sampleToken);

    // You live in an artificial world, BoxConnection. Kinda like the Truman Show.
    $this->boxConnection = new BoxConnection($this->variable, $this->oAuth2Client);
  }

  /**
   * @covers __construct()
   */
  public function testConstructor() {
    // The variables which should be set.
    $this->assertEquals('clientIdTest', $this->boxConnection->getClientId());
    $this->assertEquals('clientSecretTest', $this->boxConnection->getClientSecret());
    $this->assertEquals('https://app.box.com', $this->boxConnection->getBoxEndpoint());
    $this->assertEquals('https://api.box.com', $this->boxConnection->getBoxContentEndpoint());
    $this->assertEquals('https://localhost/', $this->boxConnection->getBoxRedirectBase());
    $this->assertEquals($this->oAuth2Client, $this->boxConnection->oAuth2Client);
    // The params array we expect from the oAuth2Client.
    $params = array(
      'auth_flow' => 'server-side',
      'client_id' => 'clientIdTest',
      'client_secret' => 'clientSecretTest',
      'token_endpoint' => 'https://app.box.com',
      'authorization_endpoint' => 'https://app.box.com',
      'redirect_uri' => 'https://localhost/',
      'scope' => NULL,
      'username' => NULL,
      'password' => NULL,
    );
    $this->assertEquals($params, $this->boxConnection->oAuth2Client->params);
  }

  /**
   * @covers authorize()
   */
  public function testAuthorize() {
    // Our test state includes a saved token in variables. Test that behavior.
    $this->boxConnection->authorize();
    $this->assertEquals($this->sampleToken, $this->boxConnection->token);

    // Setup environment for what happens when there isn't a saved token:
    // Create a mock for the Connection class, only mock the getAccessToken() method.
    $mockClient = $this->getMockBuilder('Client')
      ->setMethods(array('getAccessToken'))
      ->getMock();

    // Set up the expectation for the getAccessToken() method to be called only once.
    $mockClient->expects($this->once())
      ->method('getAccessToken');

    // Set up a new mock variable service, that returns bupkiss.
    $mockVariable = $this->getMockBuilder('\Drupal\service_container\Variable')->getMock;
    $mockVariable->method('get')->willreturn(NULL);

    // Clone our Connection object under test with no variables.
    $connectionClone = new BoxConnection($mockVariable, $mockClient);
    // Now authorize, and we should see a single call to our fake getAccessToken().
    $connectionClone->authorize();
  }

  /**
   * @covers getAuthHeader()
   */
  public function testGetAuthHeader() {
    $validReturn = array('Authorization' => 'Bearer ' . $this->sampleToken['access_token']);
    $this->assertEquals($validReturn, $this->boxConnection->getAuthHeader());
  }
}